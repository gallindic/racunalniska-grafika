class Point {
    constructor(x, y, anchorPoint){
        this.x = x;
        this.y = y;
        this.color = "#1100BB";
        this.anchorPoint = anchorPoint;
    }

    changePosition(x, y){
        this.x = x;
        this.y = y;
    }
}

class PointManager {
    static pointMove = false;
    static pointToMoveIndex = null;
    static currentCurve;

    static processMouseDown(e){
        ControlManager.setStates(e);
        let mousePosition = getMousePosition(e);

        if(CurveManager.curves.length === 0){
            CurveManager.curves.push(new Curve());
            this.currentCurve = CurveManager.curves[CurveManager.curves.length - 1];
        } 

        if(ControlManager.mouseDownState() && PointManager.intersects(mousePosition)){
            this.pointMove = true;
            this.pointToMoveIndex = this.getIndexOfPoint(mousePosition);
        }
        else{
            if(this.currentCurve.points.length === 4){
                CurveManager.curves.push(new Curve());
            
                this.currentCurve = CurveManager.curves[CurveManager.curves.length - 1];
                this.currentCurve.points.push(CurveManager.curves[CurveManager.curves.length - 2].points[3]);
            }

            PointManager.addPoints(e);
        }
    }

    static movePoint(e){
        if(this.pointMove && ControlManager.mouseDownState()){
            let mousePosition = getMousePosition(e);
            let curveIndex = this.pointToMoveIndex.i;

            if(this.pointToMoveIndex.j === 0){
                let linkedPointTranslationX = CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j + 1].x + (mousePosition.x - CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].x);
                let linkedPointTranslationY = CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j + 1].y + (mousePosition.y - CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].y);

                CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j + 1].changePosition(linkedPointTranslationX, linkedPointTranslationY);
                CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].changePosition(mousePosition.x, mousePosition.y);
            }
            else if(this.pointToMoveIndex.j === CurveManager.curvesList[curveIndex].points.length - 1){
                let linkedPointTranslationX = CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j - 1].x + (mousePosition.x - CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].x);
                let linkedPointTranslationY = CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j - 1].y + (mousePosition.y - CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].y);
                
                CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j - 1].changePosition(linkedPointTranslationX, linkedPointTranslationY);
                CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].changePosition(mousePosition.x, mousePosition.y);
            }
            else{
                CurveManager.curvesList[curveIndex].points[this.pointToMoveIndex.j].changePosition(mousePosition.x, mousePosition.y);
            }

            CurveManager.draw();
        }
    }

    static getIndexOfPoint(mousePosition){
        for(let i = 0; i < CurveManager.curvesListLength; i++){
            let curve = CurveManager.curvesList[i];

            for(let j = 0; j < curve.points.length; j++){
                if(Math.sqrt((mousePosition.x - curve.points[j].x)** 2 + (mousePosition.y - curve.points[j].y)** 2) < 10)
                    return {i: i, j: j};
            }
        }
    }

    static setPointMoveState(){
        this.pointMove = false;
    }

    static intersects(mousePosition){
        for(let i = 0; i < CurveManager.curvesListLength; i++){
            let curve = CurveManager.curvesList[i];
        
            for(let j = 0; j < curve.points.length; j++){
                if(Math.sqrt((mousePosition.x - curve.points[j].x)** 2 + (mousePosition.y - curve.points[j].y)** 2) < 10)
                    return true;
            }
        }

        return false;
    }
    
    static addPoints(e){
        let position = getMousePosition(e);

        

        if(this.currentCurve.points.length === 0){
            if(CurveManager.curvesListLength > 1){
                
                let previousControlPoint = CurveManager.curvesList[CurveManager.curvesListLength - 2].points[2];
                let anchorPoint = CurveManager.curvesList[CurveManager.curvesListLength - 2].points[3];

                let k1 = Math.abs(anchorPoint.x - position.x);
                let k2 = Math.abs(anchorPoint.y - position.y);

                let newPositionY;
                let newPositionX;
                
                if(position.y > anchorPoint.y){
                    let newPositionY = position.y - 2 * k1;
                }
                else{
                    let newPositionY = position.y + 2 * k1;
                }

                if(position.x > anchorPoint.x){
                    let newPositionX = position.x - 2 * k2;
                }
                else{
                    let newPositionX = position.x + 2 * k2;
                }

                previousControlPoint.changePosition(newPositionX, newPositionY);
            }

            this.currentCurve.addPointToTheCurve(new Point(position.x, position.y, true));
        }
        else if(this.currentCurve.points[this.currentCurve.points.length - 1].anchorPoint){
            this.currentCurve.addPointToTheCurve(new Point(position.x, position.y, false));
        }
        else if(!this.currentCurve.points[this.currentCurve.points.length - 1].anchorPoint && this.currentCurve.points[this.currentCurve.points.length - 2].anchorPoint){
            this.currentCurve.addPointToTheCurve(new Point(position.x, position.y, false));
        }
        else{
            this.currentCurve.addPointToTheCurve(new Point(position.x, position.y, true));
            CurveManager.draw();
        }
    }

    static drawControlPoint(point){
        context.beginPath();
        context.arc(point.x, point.y, 5, 0, 2 * Math.PI);
        context.fillStyle = point.color;
        context.fill();
    }

    static drawAnchorPoint(point){
        context.beginPath();
        context.rect(point.x - 5, point.y, 10, 10);
        context.fillStyle = point.color;
        context.lineWidth = 1;
        context.fill();
    }

    static drawControlLine(point1, point2){
        context.beginPath();
        context.moveTo(point1.x, point1.y);
        context.lineTo(point2.x, point2.y);
        context.lineWidth = 1;
        context.strokeStyle = "#000000";
        context.stroke();
    }
}

class Curve {
    points = [];

    constructor(color){
        this.curveColor = color || "#000000";
    }

    addPointToTheCurve(point){
        this.points.push(point);
    }

    clearCurve(){
        this.points = [];
    }
}

class CurveManager {
    static curves = [];
    static tHop = 0.01;

     static addCurveToTheList(curve){
         this.curves.push(curve);
     }

     static get curvesList(){
         return this.curves;
     }

     static get curvesListLength(){
         return this.curves.length;
     }

     static selectCurve(){
         //To-Do
     }

     static clearAllCurves(){
         this.curves = [];
     }

     static draw(){
        context.clearRect(0, 0, canvas.width, canvas.height);

        this.curves.forEach(curve=> {
            this.deCasteljau(0, curve.points[0], curve);

            curve.points.forEach((point, index) => {
                if(index == 0){
                    PointManager.drawControlLine(point, curve.points[index + 1]);
                    PointManager.drawAnchorPoint(point);
                }
                else if(!point.anchorPoint && curve.points[index + 1].anchorPoint){
                    PointManager.drawControlLine(point, curve.points[index + 1]);
                    PointManager.drawControlPoint(point)
                }
                else if(!point.anchorPoint && curve.points[index - 1].anchorPoint){
                    PointManager.drawControlPoint(point)
                }
                else if(point.anchorPoint){
                    PointManager.drawAnchorPoint(point);
                }
            });
        });
    }

    static deCasteljau(t, previousPoint, currentCurve){
        if(t >= 1)
            return;

        t += this.tHop;

        let B1 = [this.lerp(currentCurve.points[0], currentCurve.points[1], t), this.lerp(currentCurve.points[1], currentCurve.points[2], t), this.lerp(currentCurve.points[2], currentCurve.points[3], t)];
        let B2 = [this.lerp(B1[0], B1[1], t), this.lerp(B1[1], B1[2], t)];
        let B3 = this.lerp(B2[0], B2[1], t);

        this.drawCurve(B3, previousPoint, currentCurve);
        this.deCasteljau(t, B3, currentCurve);
    }

    static lerp(point1, point2, t){
        let s = 1 - t;
        return {x: point1.x * s + point2.x * t, y: point1.y * s + point2.y * t};
    }

    static drawCurve(point, previousPoint, currentCurve){
        context.beginPath();
        context.moveTo(previousPoint.x, previousPoint.y);
        context.lineTo(point.x, point.y);
        context.strokeStyle = currentCurve.curveColor;
        context.lineWidth = 2;
        context.stroke();

    }
}

class ControlManager{
    static mouseDown = false;
    
    static init(){
        document.getElementById("clearCanvas-button").addEventListener("click", this.clearCanvas);
        canvas.addEventListener("mouseup", this.setStates);
        canvas.addEventListener("mousedown", function(e) {
             PointManager.processMouseDown(e);
        });
        canvas.addEventListener("mousemove", function(e){
            e.preventDefault();
            PointManager.movePoint(e);
        });
    }

    static setStates(e){
        let type = e.type;
        
        if(type === "mousedown")
            this.mouseDown = true;
        else if(type === "mouseup"){
            this.mouseDown = false;
            PointManager.setPointMoveState();
        }
    }

    static mouseDownState(){
        return this.mouseDown;
    }

    static clearCanvas() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        curve.clearCurve();
    }
}


const canvas = document.getElementById("myCanvas");
const context = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function getMousePosition(e){
    let rect = canvas.getBoundingClientRect();
    let posx = e.clientX - rect.left;
    let posy = e.clientY - rect.top;

    return {x: posx, y: posy};
}

window.onload = function() {
    ControlManager.init();
};