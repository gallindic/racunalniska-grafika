class Vector4f {
    constructor(x, y, z, h){
        this.x = x;
        this.y = y;
        this.z = z;
        this.h = h;
    }

    static negate(vector){
        return new Vector4f(vector.x * (-1), vector.y * (-1), vector.z * (-1), vector.h);
    }

    static add(vector1, vector2){
        return new Vector4f(vector1.x + vector2.x, vector1.y + vector2.y, vector1.z + vector2.z, vector1.h);
    }

    static scalarProduct(input, vector){
        return new Vector4f(input * vector.x, input * vector.y, input * vector.z, vector.h);
    }

    static dotProduct(vector1, vector2){
        return (vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z);
    }

    static crossProduct(vector1, vector2){
        return new Vector4f(vector1.y * vector2.z - vector1.z * vector2.y, -(vector1.x * vector2.z - vector1.z * vector2.x), vector1.x * vector2.y - vector1.y * vector2.x, vector1.h);
    }

    static length(vector){
        return Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2) + Math.pow(vector.z, 2));
    }

    static normalize(vector){
        return new Vector4f(vector.x / this.length(vector), vector.y / this.length(vector), vector.z / this.length(vector), vector.h);
    }

    static project(vector1, vector2){
        let dot = this.dotProduct(vector1, vector2);
        let length = Math.pow(this.length(vector2), 2);

        return this.scalarProduct((dot / length), vector2);
    }

    static cosPhil(vector1, vector2){
        return Math.cos(this.dotProduct(vector1, vector2)) / (this.length(vector1) * this.length(vector2));
    }
}

class Matrix4f {  
    constructor(matrix = [[], [], [], []]){
        this.matrix = matrix;
    }

    static negate(matrix){
        let newMatrix = new Matrix4f();

        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 4; j++){
                newMatrix.matrix[i][j] = matrix.matrix[i][j] * (-1);
            }
        }

        return newMatrix;
    }

    static add(matrix1, matrix2){
        let newMatrix = new Matrix4f();

        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 4; j++){
                newMatrix.matrix[i][j] = matrix1.matrix[i][j] + matrix2.matrix[i][j];
            }
        }

        return newMatrix;
    }

    static transpose(matrix){
        let newMatrix = new Matrix4f();
        let diagonala = 0;

        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 4; j++){
                i == diagonala && j == diagonala ? newMatrix.matrix[i][j] = matrix.matrix[i][j] : newMatrix.matrix[j][i] = matrix.matrix[i][j];
            }

            diagonala += 1;
        }

        return newMatrix;
    }

    static multiplyScalar(input, matrix){
        let newMatrix = new Matrix4f();

        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 4; j++){
                newMatrix.matrix[i][j] = input * matrix.matrix[i][j];
            }
        }

        return newMatrix;
    }

    static multiply(matrix1, matrix2){
        let newMatrix = new Matrix4f();
        let calculation;

        for(let i = 0; i < 4; i++){
            for(let k = 0; k < 4; k++){
                calculation = 0;

                for(let j = 0; j < 4; j++){
                    calculation += matrix1.matrix[i][j] * matrix2.matrix[j][k];
                }

                newMatrix.matrix[i][k] = calculation;
            }
        }

        return newMatrix;
    }
}

class PointManager {
    constructor(){
        this.seznamTock = [];
        this.index = 0;
    }

    error(message){
        let paragraph = document.createElement("p")
        let errorMessage = document.createTextNode(message);

        paragraph.appendChild(errorMessage);
        errorContainer.appendChild(paragraph);
    }

    readValues(){
        let prebrano = textArea.value.split("v");
        errorContainer.innerHTML = "";


        for(let i = 0; i < prebrano.length; i++){
            if(prebrano[i] != ""){
                let stevila = prebrano[i].split(" ");

                if(stevila.length == 4)
                    this.seznamTock[this.index++] = new Vector4f(stevila[1], stevila[2], stevila[3], 1);
                else{
                    let vektor = "";
                    for(let j = 0; j < stevila.length; j++){
                        vektor += stevila[j] + " ";
                    }

                    this.error("Vektor v(" + vektor + ") ni ustrezal določenemu formatu");
                }
            }
        }

        textArea.value = "";

        return this.seznamTock;
    }

    writeValues(vector){
        textArea.value += "v " + (vector.x).toFixed(3) + " " + (vector.y).toFixed(3) + " " + (vector.z).toFixed(3) + "\n";
    }
}

class Transformation {
    constructor(){
        this.matrix = new Matrix4f([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]);
    }

    translate(vector){
        this.matrix.matrix[0][3] = vector.x;
        this.matrix.matrix[1][3] = vector.y;
        this.matrix.matrix[2][3] = vector.z;
    }

    scale(vector){
        this.matrix.matrix[0][0] = vector.x;
        this.matrix.matrix[1][1] = vector.y;
        this.matrix.matrix[2][2] = vector.z;
    }

    rotateX(angle){
        this.matrix.matrix[1][1] = Math.cos(angle);
        this.matrix.matrix[1][2] = -Math.sin(angle);
        this.matrix.matrix[2][1] = Math.sin(angle);
        this.matrix.matrix[2][2] = Math.cos(angle);
    }

    rotateY(angle){
        this.matrix.matrix[0][0] = Math.cos(angle);
        this.matrix.matrix[0][2] = Math.sin(angle);
        this.matrix.matrix[2][0] = -Math.sin(angle);
        this.matrix.matrix[2][2] = Math.cos(angle);
    }

    rotateZ(angle){
        this.matrix.matrix[0][0] = Math.cos(angle);
        this.matrix.matrix[0][1] = -Math.sin(angle);
        this.matrix.matrix[1][0] = Math.sin(angle);
        this.matrix.matrix[1][1] = Math.cos(angle);
    }

    transformPoint(vector){
        let point = new Transformation();
        
        let transformation = new Transformation();
        transformation.translate(new Vector4f(1.25, 0, 0));
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );

        transformation = new Transformation();
        transformation.rotateZ(Math.PI / 3);
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );

        transformation = new Transformation();
        transformation.translate(new Vector4f(0, 0, 4.15));
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );
        
        transformation = new Transformation();
        transformation.translate(new Vector4f(0, 3.14, 0));
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );
        
        transformation = new Transformation();
        transformation.scale(new Vector4f(1.12, 1.12, 1));
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );
        
        transformation = new Transformation();
        transformation.rotateY(5 * Math.PI / 8);
        point.matrix = Matrix4f.multiply(transformation.matrix, point.matrix );

        return new Vector4f(
            point.matrix.matrix[0][0] * vector.x +
            point.matrix.matrix[0][1] * vector.y +
            point.matrix.matrix[0][2] * vector.z +
            point.matrix.matrix[0][3] * vector.h,

            point.matrix.matrix[1][0] * vector.x +
            point.matrix.matrix[1][1] * vector.y +
            point.matrix.matrix[1][2] * vector.z +
            point.matrix.matrix[1][3] * vector.h,

            point.matrix.matrix[2][0] * vector.x +
            point.matrix.matrix[2][1] * vector.y +
            point.matrix.matrix[2][2] * vector.z +
            point.matrix.matrix[2][3] * vector.h,
            vector.h);
    }
}

class TransformPoints{
    constructor(){
        this.points = [];
    }

    read(){
        this.points = pointManager.readValues();
    }

    transform(){
        for(let i = 0; i < this.points.length; i++){
            pointManager.writeValues(transformacija.transformPoint(this.points[i]));
        }

        pointManager.seznamTock = [];
        pointManager.index = 0;
    }
}

const pointManager = new PointManager();
const transformacija = new Transformation();
const transformP = new TransformPoints();
const textArea = document.getElementById("field");
const errorContainer = document.getElementById("error-container");

function readButtonClick(){
    transformP.read();
}

function writeButtonClick(){
    transformP.transform();
}